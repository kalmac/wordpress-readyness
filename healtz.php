<?php

// Wordpress dies with a 500 error if database is not connected...
$dir = dirname(__FILE__);
include($dir . '/wp-config.php');

$httpCode = 200;
$httpText = 'OK';

// Checking volume
if (file_exists($dir . '/.recycled_volume')) {
  $httpCode = 409;
  $httpText = 'Volume is recycled from previous installation';
}

header($_SERVER['SERVER_PROTOCOL'] . ' ' . $httpText, true, $httpCode);
echo $httpText;
