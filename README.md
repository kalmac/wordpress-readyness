# Wordpress Readyness

A probe file for Wordpress (/healtz.php) that can used in Kubernetes

If Wordpress is OK, the file returns http code 200, else see table below :


| Code | Error |
|------|-------|
| 500  | Database is not installed or not connected |
| 409  | /var/www/html volume is recycled * |

 *Only triggered with [this Wordpress Docker image](https://gitlab.com/kalmac/wordpress)